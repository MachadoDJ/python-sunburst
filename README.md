# python-sunburst

## What does this script do?

This program reads a table in CSV format and produces a sunburst plot of the data it contains. See `example.csv` to see how to prepare the tables, and `example.pdf` for an example output file.

## What is a sunburst plot?

A sunburst plot is a series of pie charts piled up according to a predefined hierarchical structure.

## How do I execute this script?

Use your terminal to execute the script. The only parameter you need to set if the `--input` (`-i`), which should point to the file containing the data in comma-separated values (CSV). For example:

```bash
$ pythonw python-sunburst.py --input example.csv
```

The example above will create the `example.pdf` file, that comes with this script.

## What are the dependencies?

The script was tested on Unix-based operational system (not Windows). You'll need:

* [Python v3+](https://www.python.org/) (you will need a framework build of Python)
* [Matplotlib](https://matplotlib.org/) (a Python's module)
* [HPie](https://github.com/klieret/pyplot-hierarchical-pie) (a Python's module)

## Availability

The python-sunburst script is available at https://gitlab.com/MachadoDJ/python-sunburst.git.