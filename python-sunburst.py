#!/usr/bin/env pythonw
# -*- coding: utf-8 -*-

# Name: python-sunburst.py

##
# IMPORT LIBRARIES
##

import argparse, sys, re, os.path, matplotlib

try:
	import matplotlib.pyplot as plt
except:
	sys.stderr.write("! ERROR: Could not load matplotlib.\n")
	exit()
try:
	from hpie import HPie, Path, stringvalues_to_pv
except:
	sys.stderr.write("! ERROR: Could not load hpie.\n")
	exit()
else:
	fig, ax = plt.subplots()

####
## SET ARGUMENTS
####

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Input table in comma-separated values (CSV)", type = str, required = True)
args = parser.parse_args()

##
# SET UP SOME DATA
##

def setData():
	data = {}
	handle = open(args.input, "r")
	input = handle.read()
	handle.close()
	input = re.sub(r"#.*", "", input)
	input = re.sub(r"\n[\s\n]*\n", "\n", input).strip().split("\n")
	title = input[0]
	for line in input[1:]:
		values = [i.strip() for i in re.compile("[\'\"](.+?)[\'\"],").findall(line+",")]
		levels = values[:-1]
		number = values[-1]
		try:
			data["{}".format("/".join(levels))] = int(number)
		except:
			sys.stderr.write("! ERROR: The value {} should be an integer.\n".forma(number))
			exit()
	return stringvalues_to_pv(data), title

##
# CAST SPELL
##

def myHPie(data, name):
	hp = HPie(data, ax, cmap=plt.get_cmap("hsv"), plot_minimal_angle=0, label_minimal_angle=1.5) # Get labels and values
	hp.format_value_text = lambda path: "" # Remove values from labels
	hp.plot(setup_axes=True) # Configuration for X and Y-axis
	ax.set_title("{}".format(name)) # Title
	fig.tight_layout(pad=0.001) # Control size of labels and values
	fig.savefig("{}.pdf".format(name), # File name
				dpi=300, # File resolusion
				bbox_inches='tight') # Style/ formatation
	return

if __name__ == "__main__":
	# plt.show()
	# For the interpretation:
	# print("hp._completed_pv.items() = {")
	# noinspection PyProtectedMember
	# for path, value in sorted(hp._completed_pv.items(), key=lambda x: str(x[0])):
		# print("\t{}: {},".format(repr(path), value))
	# print("}")
	data, title = setData()
	myHPie(data, title)

exit()
